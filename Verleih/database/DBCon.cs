﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Verleih.database
{
    class DBCon
    {
        static private MySqlConnection con = null;
        //static private string conString = "server=127.0.0.1;user id=music;password=music;port=3306;database=musicschool"; //SslMode=Preferred
        static private string conString = "server=127.0.0.1;user id=root;password=;port=3306;database=musicschool; SslMode=Preferred";

        private DBCon() {}
        public static MySqlConnection Connection()
        {
            if (con == null) { 
                con = new MySqlConnection(conString);
                con.Open();
            }
            return con;
        }
        public static void CloseConnection()
        {
            if (con != null)
            {
                con.Close();
            }
        }

        ~DBCon()
        {
            CloseConnection();
        }

    }
}
