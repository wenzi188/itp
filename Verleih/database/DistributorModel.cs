﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Verleih.database
{
    class DistributorModel:ICRUD<Distributor>
    {
        public List<Distributor> Select(string where = "")
        {
            List<Distributor> list = new List<Distributor>();
            MySqlCommand command = DBCon.Connection().CreateCommand();
            command.CommandText = "SELECT id, name FROM Distributor where isdeleted = 0";
            if (where != "")
                command.CommandText = "SELECT id, name FROM Distributor where isdeleted = 0 and " + where;

            IDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                int id = reader.GetInt32(0);
                string name = reader[1].ToString();
                list.Add(new Distributor { id = id, Name = name });
            }

            reader.Close();
            return list;
        }

        public Distributor Read(int id)
        {
            MySqlCommand command = DBCon.Connection().CreateCommand();
            command.CommandText = "SELECT id, name FROM Distributor where isdeleted = 0 and id="+id;
            IDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                int id1 = reader.GetInt32(0);
                string name = reader[1].ToString();
                Distributor distr = new Distributor { id = id1, Name = name };
                reader.Close();
                return distr;
            }
            reader.Close();
            return null;
        }

        public Distributor Insert(Distributor distr)
        {
            string stmt = $"Insert into Distributor (name) values ('{distr.Name}')";
            MySqlCommand command = DBCon.Connection().CreateCommand();
            command.CommandText = stmt;
            command.ExecuteNonQuery();
            distr.id = (int)command.LastInsertedId;
            return distr;
        }

        public void RawDelete(int id)
        {
            Distributor distr = Read(id);
            if (distr != null)
            {
                string stmt = $"Delete from Distributor where id={id}";
                MySqlCommand command = DBCon.Connection().CreateCommand();
                command.CommandText = stmt;
                command.ExecuteNonQuery();
            }
        }

        public void Delete(int id)
        {
            Distributor distr = Read(id);
            if (distr != null)
            {
                string stmt = $"Update Distributor set isdeleted = 1 where id={id}";
                MySqlCommand command = DBCon.Connection().CreateCommand();
                command.CommandText = stmt;
                command.ExecuteNonQuery();
            }
        }


        public void Update(Distributor distr)
        {
            string stmt = $"Update Distributor set name='{distr.Name}' where id="+distr.id;
            MySqlCommand command = DBCon.Connection().CreateCommand();
            command.CommandText = stmt;
            command.ExecuteNonQuery();
        }

    }

}
