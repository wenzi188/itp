﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Verleih.database
{
    class InstrumentModel:ICRUD<Instrument>
    {
        public List<Instrument> Select(string where = "")
        {
            List<Instrument> list = new List<Instrument>();
            MySqlCommand command = DBCon.Connection().CreateCommand();
            command.CommandText = "SELECT id, name, producer, distributor_id FROM instrument where isdeleted = 0";
            if (where != "")
                command.CommandText = "SELECT id, name, producer, distributor_id FROM instrument where isdeleted = 0 and " + where;

            IDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                int id = reader.GetInt32(0);
                string name = reader[1].ToString();
                string producer = reader[2].ToString();
                int distributor_id = reader.GetInt32(3);
                list.Add(new Instrument { id = id, Name = name, Producer = producer, Distributor_id = distributor_id });
            }

            reader.Close();
            return list;
        }

        public Instrument Read(int id)
        {
            MySqlCommand command = DBCon.Connection().CreateCommand();
            command.CommandText = "SELECT id, name, producer, distributor_id FROM instrument where isdeleted = 0 and id="+id;
            IDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                string name = reader[1].ToString();
                string producer = reader[2].ToString();
                int distributor_id = reader.GetInt32(3);
                Instrument instr = new Instrument { id = id, Name = name, Producer = producer, Distributor_id = distributor_id };
                reader.Close();
                return instr;
            }
            reader.Close();
            return null;
        }

        public Instrument Insert(Instrument instr)
        {
            string stmt = $"Insert into Instrument (name, producer) values ('{instr.Name}', '{instr.Producer}')";
            if (instr.Distributor_id > -1)
                stmt = $"Insert into Instrument (name, producer, instrument_id) values ('{instr.Name}', '{instr.Producer}', '{instr.Distributor.id}')";
                
            MySqlCommand command = DBCon.Connection().CreateCommand();
            command.CommandText = stmt;
            command.ExecuteNonQuery();
            instr.id = (int)command.LastInsertedId;
            return instr;
        }

        public void RawDelete(int id)
        {
            Instrument instr = Read(id);
            if (instr != null)
            {
                string stmt = $"Delete from Instrument where id={id}";
                MySqlCommand command = DBCon.Connection().CreateCommand();
                command.CommandText = stmt;
                command.ExecuteNonQuery();
            }
        }

        public void Delete(int id)
        {
            string stmt = $"Update Instrument set isdeleted = 1 where id={id}";
            MySqlCommand command = DBCon.Connection().CreateCommand();
            command.CommandText = stmt;
            command.ExecuteNonQuery();
        }

        public void Update(Instrument instr)
        {
            string stmt = $"Update Instrument set name='{instr.Name}', producer='{instr.Producer}', distributor_id='{instr.Distributor_id}' where id=" + instr.id;
            MySqlCommand command = DBCon.Connection().CreateCommand();
            command.CommandText = stmt;
            command.ExecuteNonQuery();
        }

    }

}
