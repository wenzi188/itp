﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Verleih.database
{
    interface ICRUD<K>
    {
        List<K> Select(string where = "");
        K Read(int id);
        
        K Insert(K rec);
        
        void Delete(int id);

        void Update(K rec);

        void RawDelete(int id);
    }
}
