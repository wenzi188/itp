﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Verleih.database
{
    class InstrumentMock:ICRUD<Instrument>
    {
        private List<Instrument> liste = new List<Instrument>() { 
            new Instrument { id = 1, Name = "Trompete", Producer = "New Holland", Distributor_id = 1, IsDeleted = false },
            new Instrument { id = 2, Name = "Akkordeon", Producer = "Yamaha", Distributor_id = 2, IsDeleted = false },
            new Instrument { id = 3, Name = "Posaune", Producer = "NeuErding", Distributor_id = -1, IsDeleted = false },
            new Instrument { id = 4, Name = "Gitarre", Producer = "Yamaha", Distributor_id = 2, IsDeleted = false }
        };

        public List<Instrument> Select(string where = "")
        {
            return liste.FindAll(x => x.IsDeleted == false);
        }

        public Instrument Read(int id)
        {
            foreach (Instrument instr in liste)
                if (instr.id == id && instr.IsDeleted == false)
                    return instr;
            return null;
        }

        public Instrument Insert(Instrument instr)
        {
            int max = liste.OrderByDescending(item => item.id).First().id + 1;
            instr.id = max;
            liste.Add(instr);
            return instr;
        }

        public void RawDelete(int id)
        {
            Instrument instr = Read(id);
            if (instr != null)
                liste.Remove(instr);
        }

        public void Delete(int id)
        {
            Instrument instr = Read(id);
            if (instr != null)
                instr.IsDeleted = true;
        }

        public void Update(Instrument instr)
        {
            // not necessary because of reference
        }

    }

}
