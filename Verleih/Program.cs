﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verleih.database;

namespace Verleih
{
    class Program
    {
        static void Main(string[] args)
        {
            // aus Datenbank
            ICRUD<Instrument> im = new InstrumentModel();
            testInstrument(im);
            // aus Mock
            ICRUD<Instrument> imm = new InstrumentMock();
            testInstrument(imm);
            // aus Datenbank
            ICRUD<Distributor> dm = new DistributorModel();
            testDistributor(dm);

            // init Associations
            makeAssociations(im, dm);

        }

        static void testInstrument(ICRUD<Instrument> im)
        {
            // Model classes

            Instrument instr = new Instrument { Name = "Neue E-Gitarre 4711", Producer = "Gibson" };
            im.Insert(instr);

            List<Instrument> liste = im.Select();
            foreach (Instrument i in liste)
                Console.WriteLine(i);

            im.Delete(instr.id);

            Instrument instr1 = im.Read(4);
            instr1.Producer = "Sterling";
            im.Update(instr1);

            Console.WriteLine("--------------------------------");
            foreach (Instrument i in im.Select("producer = 'Gibson'"))
                Console.WriteLine(i);

            Console.WriteLine("Bitte Instrument ID eingeben: ");
            int id = Int32.Parse(Console.ReadLine());
            Instrument instr2 = im.Read(id);
            if (instr2 != null)
                Console.WriteLine(instr2);
            else
                Console.WriteLine($"Satz mit ID:{id} nicht gefunden!");

        }


        static void testDistributor(ICRUD<Distributor> dm)
        {
            // Model classes
            Distributor distr = new Distributor { Name = "New Holland" };
            dm.Insert(distr);

            List<Distributor> liste = dm.Select();
            foreach (Distributor i in liste)
                Console.WriteLine(i);

            dm.Delete(distr.id);

            Distributor distr1 = dm.Read(1);
            dm.Update(distr1);

            Console.WriteLine("--------------------------------");
            foreach (Distributor i in dm.Select("name = 'Musikhaus Hollerer'"))
                Console.WriteLine(i);

            Console.WriteLine("Bitte Distributor_ID eingeben: ");
            int id = Int32.Parse(Console.ReadLine());
            Distributor distr2 = dm.Read(id);
            if (distr2 != null)
                Console.WriteLine(distr2);
            else
                Console.WriteLine($"Satz mit ID:{id} nicht gefunden!");

        }



        static void makeAssociations(ICRUD<Instrument> im, ICRUD<Distributor> dm)
        {
            // Database
            List<Distributor> distributorListe = dm.Select();
            List<Instrument> liste = im.Select();

            foreach (Distributor i in distributorListe)
                Console.WriteLine(i);
            Console.WriteLine("--------------------------------------------");
            foreach (Instrument i in liste)
                Console.WriteLine(i);
            Console.WriteLine("--------------------------------------------");

            // Assoziation zu Distributor auflösen 
            foreach (Instrument i in liste)
                if (i.Distributor_id > -1)
                    i.Distributor = dm.Read(i.Distributor_id);

            foreach (Instrument i in liste)
                Console.WriteLine(i);
            Console.WriteLine("--------------------------------------------");

            // Assoziation zu Instrument auflösen
            foreach (Distributor d in distributorListe)
            {
                foreach (Instrument i in liste)
                {
                    if (i.Distributor_id == d.id && i.Distributor_id != -1)
                    {
                        d.instrumentList.Add(im.Read(i.id));
                    }
                }
            }
            foreach (Distributor i in distributorListe)
                Console.WriteLine(i);
        }
    }
}
