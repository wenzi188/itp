﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Verleih
{
    class Instrument
    {
        public int id { get; set; }
        public String Name { get; set; }

        public String Producer { get; set; }

        public int Distributor_id { get; set; } = -1;

        public Distributor Distributor { get; set; }

        public Boolean IsDeleted { get; set; } = false;

        public override string ToString()
        {
            if (Distributor == null)
                return $"ID: {id} Bezeichnung: {Name} - Hersteller: {Producer}" ;
            else
                return $"ID: {id} Bezeichnung: {Name} - Hersteller: {Producer} - Lieferant: {Distributor.Name}";
        }

    }
}
