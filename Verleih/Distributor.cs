﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Verleih
{
    class Distributor
    {
        public int id { get; set; }
        public String Name { get; set; }

        public List<Instrument> instrumentList = new List<Instrument>();

        public Boolean IsDeleted { get; set; } = false;

        public override string ToString()
        {
            string rc = $"ID: {id} Name: {Name}\n";
            if (instrumentList.Count > 0)
            {
                rc = rc + "    Liste der Instrumente:\n";
                rc = rc + "    ======================\n";
                foreach(Instrument instr in instrumentList)
                    rc = rc += $"    ID: {instr.id} Bezeichnung: {instr.Name}\n";
            }
            return rc;
        }

    }
}
